import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import csv
import argparse
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Read in the filename',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('filepath', type= str)

args = parser.parse_args()


filename= args.filepath #'proov5_correlations.npz'
#filename2 = 'proov5_divcorr.npz'
#filename3 = 'proov5_parameters.npz'

with np.load(filename[:-3] + '_correlations4.npz') as data:
    a1t = data['a1t']
    a2t = data['a2t']
    a3t = data['a3t']

with np.load(filename[:-3] + '_correlationdivision4.npz') as data2:
    #div1 = cordiv, div2 = cordiv2, div3 = cordiv3)
    div1 = data2['div1']
    div2 = data2['div2']
    div3 = data2['div3']

with np.load(filename[:-3] + '_parameters4.npz') as data3:
    # tl1, tp1, a1x, a1y
    mean = data3['mean']
    tl1 = data3['tl1']
    tp1 = data3['tp1']
    a1x = data3['a1x']
    a1y = data3['a1y']
    tl2 = data3['tl2']
    tp2 = data3['tp2']
    a2x = data3['a2x']
    a2y = data3['a2y']
    tl3 = data3['tl3']
    tp3 = data3['tp3']
    a3x = data3['a3x']
    a3y = data3['a3y']

a1t = list(a1t)
a2t = list(a2t)
a3t = list(a3t)

for index in sorted(div1, reverse=True):
    del a1t[index]

for index in sorted(div2, reverse=True):
    del a2t[index]

for index in sorted(div3, reverse=True):
    del a3t[index]


a1sum = np.mean(a1t, axis=0)
a2sum = np.mean(a2t, axis=0)
a3sum = np.mean(a3t, axis=0)

a1sum = a1sum / mean ** 2
a2sum = a2sum / mean ** 2
a3sum = a3sum / mean ** 2

# %% Diffusion
import PIL


NN = 120 # how many pixels from the center
NI = 4 # how many lines including the middle line



def diffusiondata(data, linetime, pixeltime, pixelsizex, pixelsizey, N):
    xi = np.arange(0, NN)
    #x = []
    #for i in range(N):
    #    x.append(xi)
    #x = [[x]*N]
    x = np.concatenate([xi]*N)#np.concatenate((x, x, x, x))
    x = x.astype(np.float)


    #bkiga

    yy=[]
    for i in range(N):
        yy.append(i*np.ones(NN))

    #y = yy
    y = np.concatenate(yy)#np.concatenate((0 * np.ones(NN), 1 * np.ones(NN), 2 * np.ones(NN), 3 * np.ones(NN)))
    y= y.astype(np.float)

    #print(np.array(x).shape)
    #print(np.array(y).shape)

    corr = data[data.shape[0] // 2:data.shape[0] // 2 + NN,data.shape[1] // 2:data.shape[1] // 2 + N].ravel(order="F")

    #x= np.array(x).ravel()#np.ndarray.flatten(np.array(x).astype(float).T)
    #y= np.array(y).ravel()#np.ndarray.flatten(np.array(y).astype(float).T)
    #print(x)
    #corr = np.ndarray.flatten(corr)
    #print(len(corr))
    corr = corr[1:]
    x = x[1:]
    y = y[1:]

    T2 = pixeltime * x + linetime * y

    tx = pixelsizex * x
    ty = pixelsizey * y

    T1 = np.square(tx) + np.square(ty) #tx ** 2 + ty ** 2

    return corr, T2, T1, x

#j2tan target1 ja 2 hetkel sisse
#korrelatsiooni jooned punktidena

class Task:  # N,D1,D2,tripfr,triptau,Y
    def __init__(self, T1, T2):
        self.T1 = T1
        self.T2 = T2
        self.N = []
        self.D1 = []
        self.D2 = []
        self.tripfr = []
        self.triptau = []
        self.Y = []

    def target1(self, x, N, dd, ww): #regular fit without triplet state and diffusion fraction- disabled in fit
        # w = 0.35#mikromeeter
        alpha = 4  # ilma ühikuta
        # dx = a1x #mikromeeter
        # dy = a1y #mikromeeter
        # tp = np.float(tl1) #dwell time s
        # tl = np.float(tp1) #line time s
        w = np.exp(ww)  # 0.26
        D = np.exp(dd)
        T1 = self.T1
        T2 = self.T2
        print(N, D, w)
        g = 1 / N * np.exp(-(T1) / (w ** 2 + 4 * D * np.abs(T2))) * (1 + (4 * D * (np.abs(T2))) / w ** 2) ** (-1) * (
                    1 + (4 * D * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2)
        return g

    def target2(self, x, N, dd1, dd2, ww, ttripfr, ttriptau, y): # open w parameter with triplet state - disabled in fit
        # w = 0.35#mikromeeter
        alpha = 4  # ilma ühikuta
        # dx = a1x #mikromeeter
        # dy = a1y #mikromeeter
        # tp = np.float(tl1) #dwell time s
        # tl = np.float(tp1) #line time s
        w = np.exp(ww)  # 0.26
        D1 = np.exp(dd1)  # np.exp(dd1) #np.arctan(dd1)/np.pi +0.5 # np.exp
        D2 = (np.arctan(dd2) / np.pi + 0.5) * 1000 + 1.5 * D1  # (np.arctan(dd2)/np.pi + 0.5) + 1.5*D1 #1
        triptau = np.exp(ttriptau)
        tripfr = np.arctan(ttripfr) / np.pi + 0.5
        T1 = self.T1
        T2 = self.T2
        Y = np.arctan(y) / np.pi + 0.5  # 0.1
        print(N, D1, D2, w, tripfr, triptau, Y)
        g = 1 / N * (
                (1 - Y) * np.exp(-(T1) / (w ** 2 + 4 * D1 * np.abs(T2))) * (1 + (4 * D1 * (np.abs(T2))) / w ** 2) ** (
            -1) * (1 + (4 * D1 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2) +
                (Y) * np.exp(-(T1) / (w ** 2 + 4 * D2 * np.abs(T2))) * (1 + (4 * D2 * (np.abs(T2))) / w ** 2) ** (
                    -1) * (1 + (4 * D2 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2)) * (
                    1 + tripfr * np.exp(-T2 / triptau) / (1 - tripfr))
        return g

    def target3(self, x, N, dd1, dd2, ttripfr, ttriptau, y):
        # w = 0.35#mikromeeter
        alpha = 4  # ilma ühikuta
        # dx = a1x #mikromeeter
        # dy = a1y #mikromeeter
        # tp = np.float(tl1) #dwell time s
        # tl = np.float(tp1) #line time s
        w = 0.2856  # 0.26
        D1 = np.exp(dd1)  # np.exp(dd1)#np.arctan(dd1)/np.pi+0.5
        D2 = (np.arctan(dd2) / np.pi + 0.5) * 200 + 1.5 * D1  # np.arctan(dd2)/np.pi+0.5 + 1.5*D1
        triptau = np.exp(ttriptau)
        tripfr = np.arctan(ttripfr) / np.pi + 0.5
        T1 = self.T1
        T2 = self.T2
        Y = np.arctan(y) / np.pi + 0.5

        # self.iterpara.append((N,D1,D2,tripfr,triptau,Y))
        self.N.append(N)
        self.D1.append(D1)
        self.D2.append(D2)
        self.tripfr.append(tripfr)
        self.triptau.append(triptau)
        self.Y.append(Y)

        print(N, D1, D2, w, tripfr, triptau, Y)
        g = 1 / N * ((1 - Y) * np.exp(-(T1) / (w ** 2 + 4 * D1 * np.abs(T2))) * (
                    1 + (4 * D1 * (np.abs(T2))) / w ** 2) ** (-1) * (
                                 1 + (4 * D1 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2) +
                     (Y) * np.exp(-(T1) / (w ** 2 + 4 * D2 * np.abs(T2))) * (1 + (4 * D2 * (np.abs(T2))) / w ** 2) ** (
                         -1) * (1 + (4 * D2 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2)) * (
                        1 + tripfr * np.exp(-T2 / triptau) / (1 - tripfr))
        return g  # np.asarray((n,d1,d2,tripf,tript),dtype = list)

#korr t1
#korr t2
#t1 t2
#plotid


#FCS outline

#võta aksis limiit maha

    # ploti difusiooni muutus l'bi aja

#teeb 3nda targeti fiti
def fit(corr, T2, T1):  # concatenate, T2cat, T1cat, corcat
    tgt = Task(T1, T2)  # N, dd1, dd2, ww, ttripfr, ttriptau,y

    print(T1.shape)
    print(corr.shape)

    #popt = curve_fit(tgt.target1, T1, corr, [1, 1, -1], factor=0.01, maxfev=180000)
    #popt2 = curve_fit(tgt.target2, T1, corr, [1.6096, 0.1, -3.0130, -1, -0.0087, -13.815, 1], factor=0.01,
       #               maxfev=180000)
    popt3 = curve_fit(tgt.target3, T1, corr, [1.6096, 1, -3.0130, -0.0087, -13.815, 1], factor=0.001, maxfev=180000)
    #a, bz = popt
    #b, cz = popt2
    c, dz = popt3
    v = tgt.target3(T1, *c) - corr
    res1, res2, res3 = np.array_split(v,3)


    # N,D1,D2,tripfr,triptau,Y
    N = tgt.N
    D1 = tgt.D1
    D2 = tgt.D2
    tripfr = tgt.tripfr
    triptau = tgt.triptau
    Y = tgt.Y

    plot4 = difusionplot1(a1sum, corr1, c, x, tgt1, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI, res1, 0)

    plot5 = difusionplot1(a2sum, corr2, c, x, tgt2, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI, res2, 0)
    # plot51 = difusionplot1(a2sum,a2,c2,x2,tgt2,1,'w.o. open w and solo calc')

    plot6 = difusionplot1(a3sum, corr3, c, x, tgt3, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI, res3, 1)

    para = Parameters(c)

    basedata = para.out()

    para.print(basedata, a1sum, 'Calculation for everything')

    return c, N, D1, D2, tripfr, triptau, Y, plot4, plot5, plot6, basedata


class Parameters:
    def __init__(self, c):
        #self.a = a
        #self.b = b
        self.c = c

    def out(self):
        #a = self.a
        #b = self.b
        c = self.c
        N3 = c[0]
        D13 = np.exp(c[1])  # np.exp(c[1])#np.arctan(c[1])/np.pi+0.5
        D23 = (np.arctan(c[2]) / np.pi + 0.5) * 200 + 1.5 * D13  # (np.arctan(c[2])/np.pi+0.5)*1000 + 1.5*D13
        tripfr2 = np.arctan(c[3]) / np.pi + 0.5
        triptau2 = np.exp(c[4])
        y2 = np.arctan(c[5]) / np.pi + 0.5
        p1 = (len(div1)+len(div2)+len(div3))/(len(a1t)+len(a2t)+len(a3t))
        g = np.asarray((N3, D13, D23, tripfr2, triptau2, y2,p1))
        return g

    def print(self, g, sum, text):
        print('Fit data:')
        print('N ='+ str(g[0]))
        print('Diffusion 1 =' + str(g[1]))
        print('Diffusion 2 =' + str(g[2]))
        print('Triplet fraction =' + str(g[3]))
        print('Triplet state time=' + str(g[4]))
        print('Diffusion fraction =' + str(g[5]))



corr1, t21, t11, x1 = diffusiondata(a1sum, tl1, tp1, a1x, a1y, NI)

corr2, t22, t12, x2 = diffusiondata(a2sum, tl2, tp2, a2x, a2y, NI)

corr3, t23, t13, x3 = diffusiondata(a3sum, tl3, tp3, a3x, a3y, NI)

print('p')
#print(t21.shape)
#print(corr1.shape)
print('fj')

corr = np.concatenate([corr1, corr2, corr3])

Te2 = np.concatenate([t21, t22, t23])

Te1 = np.concatenate([t11, t12, t13])

x = np.concatenate([x1, x2, x3])

#c, N, D1, D2, tripfr, triptau, Y = fit(corr, Te2, Te1)

tgt = Task(Te2, Te1)
tgt1 = Task(t11, t21)
tgt2 = Task(t12, t22)
tgt3 = Task(t13, t23)

# parm = Task.iterpara()

#para = Parameters(c)


#basedata = para.out()


def difusionplot1(data,corr, b, x1, tgt, N, text, Ni, D1, D2, tripfr, triptau, Y, NI,residual, u):

    plot, (ax1, ax2, ax3, ax4) = plt.subplots(4, squeeze='False', gridspec_kw={'height_ratios': [3, 1, 3, 3]})
    ax1.set_title(filename[-9:-3] + ' ' + text + ' '+str((data.shape[0],data.shape[1])))
    xsum = N + len(x1)
    fit = np.flip(np.array_split(np.flip(tgt.target3(x1, *b)),NI))
    xi = range(NN)
    ax1.plot(xi[1:],np.flip(fit[0]) ,label = 'Fit 4')
    for i in range(1,NI):
        ax1.plot(xi,np.flip(fit[i]), color = 'tab:blue')

    # x1 = np.arange(0,NN)
    #ax1.plot(x1, tgt.target1(x1, *a), label="Fit")
    #ax1.plot(x1[:(NI-1)*NN-1], tgt.target3(x1, *b)[:(NI-1)*NN-1], label=("Fit 4"))#, tau =" + str(round(np.exp(b[4]), 8)) + " tripfr=" + str(
        #round((np.arctan(b[3]) / np.pi + 0.5), 4))))
    corre = np.flip(np.array_split(np.flip(corr),NI))

    for i in range(NI):
        if i == 0:
            ax1.plot(xi[1:], np.flip(corre[0]),
                    label=((data.shape[0], data.shape[1]), "y=" + str(i)))
        else:
            ax1.plot(xi, np.flip(corre[i]),
                    label=((data.shape[0], data.shape[1]), "y="+str(i)))

    ax1.set(xscale = 'log', xlabel='Spatial lag (pixels)', ylabel=r"G($\xi$,y)")
    ax1.legend(loc='upper right', fontsize='small')

    for i in range(NI):
        if i ==0:
            print(i)
            ax2.plot(x1[:NN - 1],
                     residual[1:NN])

        elif i != NI-1:
            ax2.plot(x1[i*NN:(i+1) * NN - 1],
                     residual[i*NN + 1:(i+1) * NN])
            print(i)
        if i == NI-1:
            if u == 1:
                print(i)
                ax2.plot(x1[i*NN:(i+1) * NN - 1],
                        residual[i*NN:(i+1) * NN])
            else:
                print(i)
                ax2.plot(x1[i * NN:(i + 1) * NN - 1],
                         residual[i * NN:(i + 1) * NN])

    ax2.plot(x1, np.zeros(len(x1)))
    ax2.set(xscale='log')

    ax4.plot(Ni, label='N')
    ax3.plot(D1, label='D1')
    ax3.plot(D2, label='D2')
    ax4.plot(tripfr, label='tripfr')
    ax4.plot(triptau, label='triptau')
    ax4.plot(Y, label='Y')
    ax3.legend(loc='upper right', fontsize='small')
    ax4.legend(loc='upper right', fontsize='small')
    plt.subplots_adjust(left=0.1,
                        bottom=0.1,
                        right=0.9,
                        top=0.9,
                        wspace=0.4,
                        hspace=0.4)
    return plot


#korr t1
#korr t2
#t1 t2


def difplot1(data, text, T1, T2):
    plot, axs = plt.subplots(2,2,squeeze='False')#, gridspec_kw={'height_ratios': [4, 1]})
    axs[0,0].set_title(filename[-9:-3] + ' ' + text)
    axs[0,0].plot(T1, data, 'o')
    axs[1,0].plot(T2,data, 'o')
    axs[0,1].plot(T2,T1, 'o')
    axs[1,1].plot(np.sqrt(T1),data,'o')
    axs[1,1].set(xscale='log')
    return plot

c, N, D1, D2, tripfr, triptau, Y, plot1, plot2, plot3, basedata= fit(corr, Te2, Te1)

#
#
#def diagonalelem(data,el):
#    u = []
#    for i in range(len(data)):
#        a = []
#        al = data[i]
#        for r in range(al.shape[0]):
#            a.append(al[r,r])
#        u.append(a)


#plot4 = difusionplot1(a1sum, corr1,c, x, tgt1, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI,res1,0)
# plot41 = difusionplot1(a1sum,a1,c1,x1,tgt1,1,'w. chosen w and solo calc')

#plot5 = difusionplot1(a2sum,corr2, c, x, tgt2, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI,res2,0)
# plot51 = difusionplot1(a2sum,a2,c2,x2,tgt2,1,'w.o. open w and solo calc')

#plot6 = difusionplot1(a3sum, corr3, c, x, tgt3, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI,res3,1)
# plot61 = difusionplot1(a3sum,a3,c3,x3,tgt3,1,'w. chosen w and solo calc')

#plot7 = difplot1(corr,'text', Te1,Te2)

plot8 = difplot1(corr1,'Size1',t11,t21)

plot9 = difplot1(corr2,'Size2',t12,t22)

plot10 = difplot1(corr3,'Size3',t13,t23)

#plot9 = difplot1(corr,'text', Te1,Te2)

#mgoamg
# N, dd1, dd2, ww, ttripfr, ttriptau, y):

#pogpogkapkogas

#para = Parameters(c)

#para.print(basedata, a1sum, 'Calculation for everything')
# para2.print(gerta2, a1sum, 'Solo calculation for size' )
# para3.print(gerta3, a2sum, 'Solo calculation for size')
# para4.print(gerta4, a3sum, 'Solo calculation for size')

# print(g)

plt.show()



#print('Fit data:')
#print('N =' + str(g[0]))
#print('Diffusion 1 =' + str(g[1]))
#print('Diffusion 2 =' + str(g[2]))
#print('Triplet fraction =' + str(g[3]))
#print('Triplet state time=' + str(g[4]))
#print('Diffusion fraction =' + str(g[5]))
#print(len(basedata))

with open(str(filename[:-9])+ 'Proovidata4.csv' , mode = 'a') as data_file: #mode w for start, mode a for add
    data_writer = csv.writer(data_file, delimiter = ',')

    #data_writer.writerow(['Proov', 'N', 'Diffusion 1', 'Diffusion 2', 'Triplet Fraction', 'Triplet state time', 'Diffusion Fraction', 'Percentile of images thrown out'])
    data_writer.writerow([filename[-9:-3],basedata[0],basedata[1],basedata[2],basedata[3],basedata[4],basedata[5], basedata[6]])

    data_file.close()



pp = PdfPages(str(filename[:-3])+'_analyze4.pdf')
pp.savefig(plot1)
pp.savefig(plot2)
pp.savefig(plot3)
pp.close()


#Diffusionplot1 materials

# for i in range(NI):
#     if i ==0:
#         print(i)
#         ax1.plot(x1[:NN - 1],
#                  data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2][1:NN],label = 'y=' + str(i))
#     elif i != NI-1:
#         ax1.plot(x1[i*NN:(i+1) * NN - 1],
#                  data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2][
#                  i*NN + 1:(i+1) * NN],label = 'y=' + str(i))
#         print(i)
#     if i == NI-1:
#         print(i)
#         ax1.plot(x1[i*NN:(i+1) * NN - 2],
#                  data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2][
#                  i*NN + 1:(i+1) * NN],label = 'y=' + str(i))

# ax1.plot(x1, data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2],
#         label=((data.shape[0] // 2, data.shape[1] // 2), "y=0"))
# ax1.plot(x1, data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2 + 1],
#         label=((data.shape[0] // 2, data.shape[1] // 2), "y = 1"))
# ax1.plot(x1, data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2 + 2],
#         label=((data.shape[0] // 2, data.shape[1] // 2), "y = 2"))
# ax1.plot(x1, data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2 + 3],
#         label=((data.shape[0] // 2, data.shape[1] // 2), "y = 3"))

# ax2.plot(x1[:NN - 1],
#         data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2][1:NN] - tgt.target3(x1, *b)[
#                                                                                           1:NN])
# ax2.plot(x1[NN:2 * NN - 1],
#         data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2][NN + 1:2 * NN] - tgt.target3(x1,
#                                                                                                                 *b)[
#                                                                                                     NN + 1:2 * NN])
# ax2.plot(x1[2 * NN:3 * NN - 1],
#         data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2][
#         2 * NN + 1:3 * NN] - tgt.target3(x1, *b)[2 * NN + 1:3 * NN])
# ax2.plot(x1[3 * NN:4 * NN - 2],
#         data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2][
#         3 * NN + 1:4 * NN] - tgt.target3(x1, *b)[3 * NN + 1:4 * NN])
# ax2.plot(x1[4 * NN:5 * NN - 1],
#         data[data.shape[0] // 2 + N:data.shape[0] // 2 + xsum, data.shape[1] // 2][
#         4 * NN:5 * NN - 1] - tgt.target3(x1, *b)[4 * NN:5 * NN - 1])
