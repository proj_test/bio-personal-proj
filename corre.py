import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import csv
import argparse

parser = argparse.ArgumentParser(description='Read in the filename')
parser.add_argument('filepath', type= str)
args = parser.parse_args()

filename = args.filepath

#filename = 'proov5_RICS.h5'
f = h5py.File(filename, 'r')

print(str(filename[:-3]))

data = f.get("ImageStream/Confocal/Images/0")

data1 = sorted(list(data.keys()))
el = len(data1)

b = data[data1[0]].shape

protocolrics = f.get("ProtocolConfocalRICS/rics")
linetime = protocolrics["line scan time"]
flyback = protocolrics["flyback"]

a1 = []
a2 = []
a3 = []

c = 0
d = 0


def padding(data):
    u = []
    for i in range(len(data)):
        paddeddata = np.pad(data[i], (
        (data[i].shape[0] // 2, data[i].shape[0] // 2), (data[i].shape[1] // 2, data[i].shape[1] // 2)),
                            constant_values=0.0)
        u.append(paddeddata)
    return u


def calcmean(data1, data2, data3):
    # Sum all the groups
    sum1 = np.sum(data1)
    sum2 = np.sum(data2)
    sum3 = np.sum(data3)
    # b,c,d lists using the shapes of a1,a2,a3
    leng = data1[0].shape[0] * data1[0].shape[1] * len(data1) + data2[0].shape[0] * data2[0].shape[1] * len(data2) + \
           data3[0].shape[0] * data3[0].shape[1] * len(data3)
    mean = (sum1 + sum2 + sum3) / leng
    return mean


def factorcalc(data):
    yhed = np.ones((data[0].shape[0] // 2, data[0].shape[1] // 2))
    yhed = np.pad(yhed, ((yhed.shape[0] // 2, yhed.shape[0] // 2), (yhed.shape[1] // 2, yhed.shape[1] // 2)),
                  constant_values=0.0)
    factor = signal.correlate(yhed, yhed, mode="same")
    return factor


def correlationcalc(data, factor):
    at = []
    for i in range(len(data)):
        corre = signal.correlate(data[i], data[i], mode="same")
        corre = corre / factor
        at.append(corre)
    return at


def picturemean(data):
    picturemean = np.mean(data, axis=0)
    return picturemean


def dataset(size):
    a1 = []
    for i in range(el):
        if data[data1[i]].shape == size:
            pixelsizex = data[data1[i]].attrs["PixelSizeX"]
            pixelsizey = data[data1[i]].attrs["PixelSizeY"]
            linet = linetime[i]
            dataset = data[data1[i]][:].astype(float).T
            pixelt = linet / dataset.shape[0]
            linet = linet * (1 + flyback[i])
            a1.append(dataset)
    return a1, linet, pixelt, pixelsizex, pixelsizey


def datasize():
    a = 0
    shape1 = data[data1[0]].shape
    for i in range(el):
        if data[data1[i]].shape == shape1:
            a = i
        else:
            shape2 = data[data1[i]].shape
            break

    for i in range(a,el):
        if data[data1[i]].shape == shape2:
            a = i
        elif data[data1[i]].shape == shape1:
            a = i
        else:
            shape3 = data[data1[i]].shape
            break
    # for i in range(a,el):
    # if data[data1[i]].shape == shape2:
    #    a += i
    # else:
    #  shape3 = data[data1[i]].shape
    # break
    print(filename)
    return shape1, shape2, shape3

def blockshaped(arr, nrows, ncols):
    """
    Return an array of shape (n, nrows, ncols) where
    n * nrows * ncols = arr.size

    If arr is a 2D array, the returned array should look like n subblocks with
    each subblock preserving the "physical" layout of arr.
    """
    h, w = arr.shape
    assert h % nrows == 0, "{} rows is not evenly divisble by {}".format(h, nrows)
    assert w % ncols == 0, "{} cols is not evenly divisble by {}".format(w, ncols)
    return (arr.reshape(h//nrows, nrows, -1, ncols)
               .swapaxes(1,2)
               .reshape(-1, nrows, ncols))

def datasmaller(data, N):
    u = []
    for i in range(len(data)):
        if N % 2 != 0:
            u.append(data)
            break
        a = data[i]
        c = np.vsplit(a,2)
        b1,b11 = np.hsplit(c[0],2)
        b2,b22 = np.hsplit(c[1],2)
        u.append(b1)
        u.append(b2)
        u.append(b11)
        u.append(b22)
    return u

sh1, sh2, sh3 = datasize()

a1, tl1, tp1, a1x, a1y = dataset(sh1)
a2, tl2, tp2, a2x, a2y = dataset(sh2)
a3, tl3, tp3, a3x, a3y = dataset(sh3)

print(len(a1))

a1 = datasmaller(a1,4)
a2 = datasmaller(a2,4)
a3 = datasmaller(a3,4)

print(len(a1[0]))

mean = calcmean(a1, a2, a3)

picmean1 = picturemean(a1)
picmean2 = picturemean(a2)
picmean3 = picturemean(a3)

a1 = a1 - picmean1
a2 = a2 - picmean2
a3 = a3 - picmean3

a1 = padding(a1)
a2 = padding(a2)
a3 = padding(a3)

# print(mean)

# listi alguseks
a1t = []
a2t = []
a3t = []

factor = factorcalc(a1)
factor1 = factorcalc(a2)
factor2 = factorcalc(a3)

a1t = correlationcalc(a1, factor)
a2t = correlationcalc(a2, factor1)
a3t = correlationcalc(a3, factor2)

correlations = [a1t,a2t,a3t]

np.savez(str(filename[:-3])+'_correlations4', a1t = a1t, a2t = a2t, a3t = a3t )
np.savez(str(filename[:-3])+'_parameters4', mean = mean, tl1 = tl1, tp1 = tp1, a1x = a1x, a1y = a1y,
         tl2 = tl2, tp2= tp2, a2x = a2x, a2y = a1y, tl3 = tl3, tp3 = tp3, a3x = a3x, a3y = a3y)
