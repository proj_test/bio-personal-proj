import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import csv
import argparse
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

parser = argparse.ArgumentParser(description='Read in the filename')
parser.add_argument('filepath', type= str)
args = parser.parse_args()

filename = args.filepath

with np.load(filename[:-3] + '_correlations4.npz') as data:
    a1t = data['a1t']
    a2t = data['a2t']
    a3t = data['a3t']

with np.load(filename[:-3] + '_correlationdivision4.npz') as data2:
    #div1 = cordiv, div2 = cordiv2, div3 = cordiv3)
    div1 = data2['div1']
    div2 = data2['div2']
    div3 = data2['div3']

#filename = 'proov5_RICS.h5'
f = h5py.File(filename, 'r')

print(str(filename[:-3]))

data = f.get("ImageStream/Confocal/Images/0")

data1 = sorted(list(data.keys()))
el = len(data1)

b = data[data1[0]].shape

protocolrics = f.get("ProtocolConfocalRICS/rics")
linetime = protocolrics["line scan time"]
flyback = protocolrics["flyback"]

a1 = []
a2 = []
a3 = []

c = 0
d = 0

def datasize():
    a = 0
    shape1 = data[data1[0]].shape
    for i in range(el):
        if data[data1[i]].shape == shape1:
            a = i
        else:
            shape2 = data[data1[i]].shape
            break

    for i in range(a,el):
        if data[data1[i]].shape == shape2:
            a = i
        elif data[data1[i]].shape == shape1:
            a = i
        else:
            shape3 = data[data1[i]].shape
            break
    # for i in range(a,el):
    # if data[data1[i]].shape == shape2:
    #    a += i
    # else:
    #  shape3 = data[data1[i]].shape
    # break
    print(filename)
    return shape1, shape2, shape3

def dataset(size):
    a1 = []
    for i in range(el):
        if data[data1[i]].shape == size:
            pixelsizex = data[data1[i]].attrs["PixelSizeX"]
            pixelsizey = data[data1[i]].attrs["PixelSizeY"]
            linet = linetime[i]
            dataset = data[data1[i]][:].astype(float).T
            pixelt = linet / dataset.shape[0]
            linet = linet * (1 + flyback[i])
            a1.append(dataset)
    return a1, linet, pixelt, pixelsizex, pixelsizey

sh1, sh2, sh3 = datasize()

a1, tl1, tp1, a1x, a1y = dataset(sh1)
a2, tl2, tp2, a2x, a2y = dataset(sh2)
a3, tl3, tp3, a3x, a3y = dataset(sh3)

def centerlist(list1,div):
    u = []
    l = []
    for i in range(len(list1)):
        if i in div:
            l.append(i)
            a = list1[i]
            u.append(a[a.shape[0]//2-300:a.shape[0]//2+300,a.shape[1]//2-300:a.shape[1]//2+300])
        else:
            l.append(-1)
            a = list1[i]
            u.append(a[a.shape[0] // 2 - 300:a.shape[0] // 2 + 300, a.shape[1] // 2 - 300:a.shape[1] // 2 + 300])
    return u, l

def vmax(list1):
    u = []
    for i in range(len(list1)):
        u.append(np.percentile(np.ndarray.flatten(list1[i]),95))
    return u

a1list, a1count = centerlist(a1t,div1)
a2list, a2count = centerlist(a2t,div2)
a3list, a3count = centerlist(a3t,div3)

print(a1count)

vmax1 = vmax(a1list)
vmax2 = vmax(a2list)
vmax3 = vmax(a3list)

def showimage(list1,list2,div,y,vmax, count):
    pp = PdfPages(str(filename[:-3]) +str(y)+ 'allimages4.pdf')

    #for i in range(len(list1)):
    #    if i in div:
    #       continue
    #    else:
    #        fig3 = plt.figure(constrained_layout=True)
    #        gs = fig3.add_gridspec(2, 1)
    #        f3_ax1 = fig3.add_subplot(gs[0, :])
    #        f3_ax1.set_title('Size ' + str(list1[i].shape[0]) + 'x' + str(list1[i].shape[1]) +
    #                         ' normal plot, index ' + str(i) +' '+ str(min(np.ndarray.flatten(list1[i]))) + ', '+
    #                         str(max(np.ndarray.flatten(list1[i]))))
    #        f3_ax1.imshow(list1[i], vmin=min(np.ndarray.flatten(list1[i])), vmax=max(np.ndarray.flatten(list1[i])))
    #        #divider = make_axes_locatable(p1)
            #cax = divider.append_axes('right', size='5%', pad=0.05)
            #fig3.colorbar(p1, cax=cax, orientation='vertical')

    #        f3_ax2 = fig3.add_subplot(gs[1, :])
    #        f3_ax2.set_title(str(min(np.ndarray.flatten(list2[i]))) + ', '+
    #                         str(np.percentile(np.ndarray.flatten(list2[i]), 95)))
    #        f3_ax2.imshow(list2[i], vmin=min(np.ndarray.flatten(list2[i])), vmax=np.percentile(np.ndarray.flatten(list2[i]),95))
            #divider = make_axes_locatable(p2)
            #cax = divider.append_axes('right', size='5%', pad=0.05)
            #fig3.colorbar(p2, cax=cax, orientation='vertical')

    #        pp.savefig(fig3)
    #        plt.close()
    #        break


#600x600 plot korrelatsiooni keskest
#percentile, annab 1 tulemuse, flatteni korr data ja arvuta, kasuta vmaxina

    for i in range(len(list1)):
        if count[i] == -1:
            fig3 = plt.figure(constrained_layout=True)
            gs = fig3.add_gridspec(2, 1)
            f3_ax1 = fig3.add_subplot(gs[0, :])
            f3_ax1.set_title('Size ' + str(list1[i].shape[0]) + 'x' + str(list1[i].shape[1]) + ' normal plot index ' + str(i) +
                             ' ' + str(min(np.ndarray.flatten(list1[i]))) + ', ' +
                             str(max(np.ndarray.flatten(list1[i]))))
            f3_ax1.imshow(list1[i], vmin=min(np.ndarray.flatten(list1[i])), vmax=max(np.ndarray.flatten(list1[i])))
            # divider = make_axes_locatable(p1)
            # cax = divider.append_axes('right', size='5%', pad=0.05)
            # fig3.colorbar(p1, cax=cax, orientation='vertical')

            f3_ax2 = fig3.add_subplot(gs[1, :])
            f3_ax2.set_title(str(min(np.ndarray.flatten(list2[i]))) + ', ' +
                             str(vmax[i]))
            f3_ax2.imshow(list2[i], vmin=min(np.ndarray.flatten(list2[i])), vmax=vmax[i])
            # divider = make_axes_locatable(p2)
            # cax = divider.append_axes('right', size='5%', pad=0.05)
            # fig3.colorbar(p2, cax=cax, orientation='vertical')

            pp.savefig(fig3)
            plt.close()
        # di = plt.figure(str(i))
        # plt.imshow(list1[i])
        # pp.savefig(di)
        # fi = plt.figure(str(i)+'_1')
        # plt.imshow(list2[i],vmin=0, vmax = 0.005)
        # pp.savefig(fi)
        else:
            fig3 = plt.figure(constrained_layout=True)
            gs = fig3.add_gridspec(2, 1)
            f3_ax1 = fig3.add_subplot(gs[0, :])
            f3_ax1.set_title('Size ' + str(list1[i].shape[0])+'x'+ str(list1[i].shape[1])+' removed plot index '+str(i)+
                             ' '+ str(min(np.ndarray.flatten(list1[i]))) + ', '+
                             str(max(np.ndarray.flatten(list1[i]))))
            f3_ax1.imshow(list1[i],vmin = min(np.ndarray.flatten(list1[i])), vmax = max(np.ndarray.flatten(list1[i])))
            #divider = make_axes_locatable(p1)
            #cax = divider.append_axes('right', size='5%', pad=0.05)
            #fig3.colorbar(p1, cax=cax, orientation='vertical')

            f3_ax2 = fig3.add_subplot(gs[1, :])
            f3_ax2.set_title(str(min(np.ndarray.flatten(list2[i]))) + ', ' +
                         str(vmax[i]))
            f3_ax2.imshow(list2[i], vmin = min(np.ndarray.flatten(list2[i])), vmax = vmax[i])
        #divider = make_axes_locatable(p2)
        #cax = divider.append_axes('right', size='5%', pad=0.05)
            #fig3.colorbar(p2, cax=cax, orientation='vertical')

            pp.savefig(fig3)
            plt.close()
       # di = plt.figure(str(i))
       # plt.imshow(list1[i])
       # pp.savefig(di)
       # fi = plt.figure(str(i)+'_1')
       # plt.imshow(list2[i],vmin=0, vmax = 0.005)
       # pp.savefig(fi)
    pp.close()
print(len(a1count))
print(len(a1list))
print(len(a1))
showimage(a1,a1list,div1,1,vmax1, a1count)
showimage(a2,a2list,div2,2,vmax2, a2count)
showimage(a3,a3list,div3,3,vmax3, a3count)


