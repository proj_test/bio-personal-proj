import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import csv
import argparse
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Read in the filename',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('filepath', type= str)
parser.add_argument('y11', metavar = 'N.N', type= float, nargs = '?', const = -1, default =-1)
parser.add_argument('y21', metavar = 'N.N',type= float, nargs = '?', const = -1, default =-1)
parser.add_argument('y31', metavar = 'N.N',type= float, nargs = '?', const = -1, default =-1)
#parser.add_argument('y2y0_2', metavar = 'N.N',type= float, nargs = '?', const = -1, default =-1)
#parser.add_argument('y1y0_3', metavar = 'N.N',type= float, nargs = '?', const = -1, default =-1)
#parser.add_argument('y2y0_3', metavar = 'N.N',type= float, nargs = '?', const = -1, default =-1)

args = parser.parse_args()


filename = args.filepath #'proov5_correlations.npz'
y11,y21,y31 = [args.y11,args.y21,args.y31]#args.y2y0_2,args.y1y0_3,args.y2y0_3]

#moga

with np.load(filename[:-3]+'_correlations4.npz') as data:
    a1t = data['a1t']
    a2t = data['a2t']
    a3t = data['a3t']


print('gngi')
print(len(a1t))
print(len(a2t))
print(len(a3t))

def boxcorr(corr,y):
    box = []
    for i in range(len(corr)):
        a = corr[i]
        box.append(a[a.shape[0]//2-y:a.shape[0]//2+y,a.shape[1]//2-y:a.shape[1]//2+y])
    return box

box1 = boxcorr(a1t, 500)
box2 = boxcorr(a2t, 500)
box3 = boxcorr(a3t, 500)

def removepercentile(box, border):
    u = []
    for i in range(len(box)):
        b = np.percentile(box[i],95)
        if b > border:
            u.append(i)
        else:
            continue
    return u

prec1 = y11*10**(-5)
prec2 = y21*10**(-5)
prec3 = y31*10**(-5)


ind1 = removepercentile(box1,prec1)#8.9*10**(-5))
ind2 = removepercentile(box2,prec2)#7.0*10**(-5))
ind3 = removepercentile(box3,prec3)#8.4*10**(-5))


#proov4 3000 yle
#proov2 1732 yle vaadata


def corrpercentile(box,corr, index):
    u = []
    b = []
    for i in range(len(box)):
        if i in index:
            u.append(np.percentile(box[i],95))
            b.append(np.percentile(box[i],95))
        else:
            u.append(np.percentile(box[i],95))
            b.append(0)

    fig = plt.figure()
    plt.title(str(corr[0].shape[0])+','+str(corr[0].shape[1]))
    plt.plot(u,'o')
    plt.plot(b,'o',label='remove')
    plt.xlabel('index')
    plt.ylabel('percentile')
    plt.yscale('log')

    return fig

#kuhu langeb scatter v'ljalase perc
#kuhu langeb percentile v'ljalase scatter

fig1 = corrpercentile(box1,a1t,ind1)
fig2 = corrpercentile(box2,a2t,ind2)
fig3 = corrpercentile(box3,a3t,ind3)

def corrdiv(corr,y, n,arg1, arg2, indexes):
    div1 = []
    div2 = []
    div3 = []
    div4 = []
    div0 = []
    for i in range(len(corr)):
        a = corr[i]
        if i in indexes:

            div1.append(a[a.shape[0] // 2 + y, a.shape[1] // 2 + 1] / a[a.shape[0] // 2 +y, a.shape[1] // 2])
            div2.append(a[a.shape[0] // 2 + y, a.shape[1] // 2 + 2] / a[a.shape[0] // 2 +y, a.shape[1] // 2])
            div3.append(a[a.shape[0] // 2 + y, a.shape[1] // 2 + 1] / a[a.shape[0] // 2 +y, a.shape[1] // 2])
            div4.append(a[a.shape[0] // 2 + y, a.shape[1] // 2 + 2] / a[a.shape[0] // 2 +y, a.shape[1] // 2])
        else:

            div1.append(a[a.shape[0] // 2 + y, a.shape[1] // 2 + 1] / a[a.shape[0] // 2 + y, a.shape[1] // 2])
            div2.append(a[a.shape[0] // 2 + y, a.shape[1] // 2 + 2] / a[a.shape[0] // 2 + y, a.shape[1] // 2])
            div3.append(0)
            div4.append(0)
        if div1[i]<0 or div2[i]<0:
            div0.append(i)
    if arg1 < 0 and arg2 < 0:
        fig3 = plt.figure('div1/div2'+str(corr[0].shape))
        plt.title('Size '+ str(n) + ' ' + str(corr[0].shape[0]//2)+'x'+str(corr[0].shape[1]//2))
        plt.plot(div2,div1,'o')
        plt.plot(div4,div3,'o',label='index')
        plt.plot(np.zeros_like(np.arange(0, 1.5, 0.1)), np.arange(0, 1.5, 0.1))
        plt.plot(np.arange(0, 1.5, 0.1), np.zeros_like(np.arange(0, 1.5, 0.1)))
        plt.xlabel('y2/y0')
        plt.ylabel('y1/y0')
        plt.legend()
    else:
        fig3 = plt.figure(constrained_layout=True)
        gs = fig3.add_gridspec(2, 3)
        f3_ax1 = fig3.add_subplot(gs[0, :])
        f3_ax1.set_title('Size ' + str(n) + ' ' + str(corr[0].shape[0] // 2) + 'x' + str(corr[0].shape[1] // 2))
        f3_ax1.plot(div2, div1, 'o')
        f3_ax1.plot(np.ones_like(np.arange(0,max(div2),0.1)) * arg2, np.arange(0,max(div2),0.1))
        f3_ax1.plot(np.zeros_like(np.arange(0,1.5,0.1)), np.arange(0,1.5,0.1))
        f3_ax1.plot(np.arange(0,1.5,0.1), np.zeros_like(np.arange(0, 1.5, 0.1)))
        f3_ax1.plot(div2, np.ones(len(div1)) * arg1)
        f3_ax1.set( xlabel = 'y2/y0' , ylabel='y1/y0')

        f3_ax2 = fig3.add_subplot(gs[1, :])
        f3_ax2.set_axis_off()
        f3_ax2.text(0, 0.5, r'y2/y0 =' + str(arg2))
        f3_ax2.text(0, 0.6, r'y1/y0 =' + str(arg1))

    print(np.mean(div1))
    print(np.mean(div2))
    #plt.show()
    return div1, div2, div0, fig3

def corrdivcheck(corr, y, m1, m2):
    u = []
    for i in range(len(corr)):
        a = corr[i]
        b = a[a.shape[0]//2+y, a.shape[1]//2+1] / a[a.shape[0]//2+y, a.shape[1]//2] # 1/0
        c = a[a.shape[0] // 2 + y, a.shape[1] // 2 + 2] / a[a.shape[0] // 2 + y, a.shape[1] // 2] #2/0
        if b > m1 or c > m2 or b<0 or c<0:
            u.append(i)
    print(u)
    return u

i1 = [0,1,12,20,21,24,25,27,28,29,30,31, 73, 79, 89, 96, 100, 111, 116, 119, 129,135  ]
i2 = [3, 10 ,25, 31 , 33, 43, 55 , 89, 90 ,97]
i3 = [2 , 28 , 40, 44,52, 53 ,54 ]


d1, d2, d01,a1 = corrdiv(a1t,10, 1, -1,-1,ind1)
d3, d4, d02,a2 = corrdiv(a2t,10, 2, -1,-1,ind2)
d5, d6, d03,a3 = corrdiv(a3t,10, 3, -1,-1,ind3)

plt.show()

#cordiv = corrdivcheck(a1t,10,-1,-1)
#cordiv2 = corrdivcheck(a2t,10,args.y1y0_2,args.y2y0_2)
#cordiv3 = corrdivcheck(a3t,10,args.y1y0_3,args.y2y0_3)

print(d01)
print(d02)
print(d03)

np.savez(str(filename[:-3])+'_correlationdivision4', div1 = ind1, div2 = ind2, div3 = ind3,
         p1 = len(ind1)/len(a1t), p2 = len(ind2)/len(a2t),
         p3 = len(ind3)/len(a3t))

with open(str(filename[:-3])+'4.csv', 'a', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow([prec1,prec2,prec3])#args.y1y0_1,args.y2y0_1, args.y1y0_2, args.y2y0_2 , args.y1y0_3, args.y2y0_3])
    #writer.writerow([args.y1y0_2, args.y2y0_2])
    #writer.writerow([args.y1y0_3, args.y2y0_3])

pp = PdfPages(str(filename[:-3])+'.pdf')
pp.savefig(a1)
pp.savefig(a2)
pp.savefig(a3)
pp.close()

pp = PdfPages(str(filename[:-3])+'percentile.pdf')
pp.savefig(fig1)
pp.savefig(fig2)
pp.savefig(fig3)
pp.close()
